<html>
<head>
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
    <title>Users</title>
</head>
<body>
<div class="tableCont">
    <table>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
        </tr>
        <#list usersFromServer as user>
            <tr>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
            </tr>
        </#list>
    </table>
</div>
</body>
</html>
