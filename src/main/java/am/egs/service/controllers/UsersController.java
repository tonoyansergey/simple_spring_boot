package am.egs.service.controllers;

import am.egs.service.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;

@Controller
public class UsersController {

    @Autowired
    DataSource dataSource;

    @Autowired
    private UsersRepository usersRepository;


    @GetMapping("/")
    public String getUsersPage(ModelMap model) {
        model.addAttribute("usersFromServer", usersRepository.findAll());
        return "users";
    }
}
