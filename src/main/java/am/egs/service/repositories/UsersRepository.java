package am.egs.service.repositories;

import am.egs.service.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {

    List<User> findAllByFirstName(String firstName);
}
